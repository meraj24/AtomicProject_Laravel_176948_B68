<?php

namespace App\Http\Controllers;

use App\BirthDay;
use Illuminate\Http\Request;

class BirthDayController extends Controller
{
    public function store(){

        $objModel = new BirthDay();
        $objModel->name=$_POST['name'];
        $objModel->birthday=$_POST['birthday'];
        $status=$objModel->save();
        return redirect()->route('BirthDayCreate');
    }
    public function index(){

        $objBirthDayModel = new BirthDay();

        $allData = $objBirthDayModel->paginate(5);


        return view("Birth_Day/index",compact('allData'));

    }



    public function view($id){


        $objBirthDayModel = new BirthDay();


        $oneData = $objBirthDayModel->find($id);

        return view('Birth_Day/view',compact('oneData'));

    }




    public function edit($id){


        $objBirthDayModel = new BirthDay();

        $oneData = $objBirthDayModel->find($id);

        return view('Birth_Day/edit',compact('oneData'));
    }




    public function update(){


        $objBirthDayModel = new BirthDay();

        $oneData = $objBirthDayModel->find($_POST['id']);
        $oneData->name = $_POST["name"];
        $oneData->birthday = $_POST["birthday"];

        $status =  $oneData->update();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('birthdayindex');


    }



    public function delete($id){


        $objBirthDayModel = new BirthDay();

        $status = $objBirthDayModel->find($id)->delete();

        if($status) echo "success!";
        else echo "failed";

        return redirect()->route('birthdayindex');

    }


    public function search($keyword){



        $objBirthDayModel = new BirthDay();

        $searchResult =  $objBirthDayModel
            ->where("name","LIKE","%$keyword%")
            ->orwhere("birthday","LIKE","%$keyword%")
            ->paginate(5);


        return view('Birth_Day/search_result',compact('birthdaysearchResult')) ;

    }

}
