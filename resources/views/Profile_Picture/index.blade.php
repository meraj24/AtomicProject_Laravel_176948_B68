@extends('../master')


@section('title','Profile Picture - Active List')


@section('content')


    <div class="container">

        <div class="navbar">

            <a href="/profile_picture"><button type="button" class="btn btn-primary">Add New</button></a>
        </div>

        {!! Form::open(['url'=>'Profile_Picture/search_result']) !!}


        {!! Form::text('keyword') !!}
        {!! Form::submit('Search',['class'=> 'btn btn-success']) !!}

        {!! Form::close() !!}




        Total: {!! $allData->total() !!} Profile Pictures(s) <br>

        Showing: {!! $allData->count() !!} Profile Pictures(s) <br>

        {!! $allData->links() !!}




        <table class="table table-bordered table table-striped" >

            <th>name</th>
            <th>profile picture</th>

            <th>Action Buttons</th>

            @foreach($allData as $oneData)

                <tr>

                    <td>  {!! $oneData['name'] !!} </td>
                    <td>  {!! $oneData['profile_picture'] !!} </td>


                    <td>
                        <a href="view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                        <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                        <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                    </td>

                </tr>


            @endforeach


        </table>
        {!! $allData->links() !!}
    </div>



@endsection