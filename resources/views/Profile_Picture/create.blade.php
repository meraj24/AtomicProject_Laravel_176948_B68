@extends('../master')

@section('title','Profile picture - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Profile Picture - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Profile_Picture/store']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('profile_picture','Profile Picture:') !!}
            {!! Form::file('profile_picture') !!}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection