@extends('../master')


@section('title','city - Active List')


@section('content')


    <div class="container">

        <div class="navbar">

            <a href="/city"><button type="button" class="btn btn-primary btn-lg">Add New</button></a>
        </div>

        {!! Form::open(['url'=>'City/search_result']) !!}


        {!! Form::text('keyword') !!}
        {!! Form::submit('Search',['class'=> 'btn btn-success']) !!}

        {!! Form::close() !!}




        Total: {!! $allData->total() !!} Book(s) <br>

        Showing: {!! $allData->count() !!} Book(s) <br>

        {!! $allData->links() !!}




        <table class="table table-bordered table table-striped" >

            <th>City</th>

            <th>Action Buttons</th>

            @foreach($allData as $oneData)

                <tr>

                    <td>  {!! $oneData['city'] !!} </td>


                    <td>
                        <a href="view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                        <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                        <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                    </td>

                </tr>


            @endforeach


        </table>
        {!! $allData->links() !!}
    </div>



@endsection