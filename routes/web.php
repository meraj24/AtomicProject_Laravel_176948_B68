<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');






Route::get('/booktitle', function () {
    return view('Book_Title/create');
});

Route::post('/Book_Title/store', ['uses'=>'BookTitleController@store']);

Route::get('/BookTitle/create',function (){

    return view('Book_Title/create');
} )->name('BookTitleCreate');

Route::get('/Book_Title/index', 'BookTitleController@index')->name('bookindex');

Route::get('/Book_Title/view/{id}', ['uses'=>'BookTitleController@view']);

Route::get('/Book_Title/edit/{id}', 'BookTitleController@edit');

Route::post('/Book_Title/update', ['uses'=>'BookTitleController@update']);

Route::get('/Book_Title/delete/{id}', ['uses'=>'BookTitleController@delete']);

Route::post('Book_Title/search_result', function(){

    $path = 'Book_Title/search/'. $_POST['keyword'];

    return redirect($path);

});

Route::get('/Book_Title/search/{keyword}', ['uses'=>'BookTitleController@search'])->name('searchindex');


Route::get('/Book_Title/search/',function (){
    return redirect()->route('searchindex');
});



Route::get('/BookTitle/',function (){
    return redirect()->route('bookindex');
});







Route::get('/birthday', function () {
    return view('Birth_Day/create');
});

Route::post('/Birth_Day/store', ['uses'=>'BirthDayController@store']);

Route::get('/Birth_Day/create',function (){

    return view('Birth_Day/create');
} )->name('BirthDayCreate');
Route::get('/Birth_Day/index', 'BirthDayController@index')->name('birthdayindex');

Route::get('/Birth_Day/view/{id}', ['uses'=>'BirthDayController@view']);

Route::get('/Birth_Day/edit/{id}', 'BirthDayController@edit');

Route::post('/Birth_Day/update', ['uses'=>'BirthDayController@update']);

Route::get('/Birth_Day/delete/{id}', ['uses'=>'BirthDayController@delete']);

Route::post('Birth_Day/search_result', function(){

    $path = 'Birth_Day/search/'. $_POST['keyword'];

    return redirect($path);

});

Route::get('/Birth_Day/search/{keyword}', ['uses'=>'BirthDaController@search'])->name('birthdaysearchindex');


Route::get('/Birth_Day/search/',function (){
    return redirect()->route('birthdaysearchindex');
});



Route::get('/Birth_Day/',function (){
    return redirect()->route('birthdayindex');
});








Route::get('/city', function () {
    return view('City/create');
});
Route::post('/City/store', ['uses'=>'CityController@store']);

Route::get('/City/create',function (){

    return view('City/create');
} )->name('CityCreate');

Route::get('/City/index', 'CityController@index')->name('cityindex');

Route::get('/City/view/{id}', ['uses'=>'CityController@view']);

Route::get('/City/edit/{id}', 'CityController@edit');

Route::post('/City/update', ['uses'=>'CityController@update']);

Route::get('/City/delete/{id}', ['uses'=>'CityController@delete']);

Route::post('City/search_result', function(){

    $path = 'City/search/'. $_POST['keyword'];

    return redirect($path);

});

Route::get('/City/search/{keyword}', ['uses'=>'BookTitleController@search'])->name('citysearchindex');


Route::get('/City/search/',function (){
    return redirect()->route('citysearchindex');
});



Route::get('/City/',function (){
    return redirect()->route('cityindex');
});












Route::get('/email', function () {
    return view('Email/create');
});

Route::post('/Email/store', ['uses'=>'EmailController@store']);

Route::get('/Email/create',function (){

    return view('Email/create');
} )->name('EmailCreate');

Route::get('/Email/index', 'EmailController@index')->name('emailindex');

Route::get('/Email/view/{id}', ['uses'=>'EmailController@view']);

Route::get('/Email/edit/{id}', 'EmailController@edit');

Route::post('/Email/update', ['uses'=>'EmailController@update']);

Route::get('/Email/delete/{id}', ['uses'=>'EmailController@delete']);

Route::post('Email/search_result', function(){

    $path = 'Email/search/'. $_POST['keyword'];

    return redirect($path);

});

Route::get('/Email/search/{keyword}', ['uses'=>'EmailController@search'])->name('emailsearchindex');


Route::get('/Email/search/',function (){
    return redirect()->route('searchindex');
});



Route::get('/Email/',function (){
    return redirect()->route('emailindex');
});













Route::get('/gender', function () {
    return view('Gender/create');
});

Route::post('/Gender/store', ['uses'=>'GenderController@store']);

Route::get('/Gender/create',function (){

    return view('Gender/create');
} )->name('GenderCreate');

Route::get('/Gender/index', 'GenderController@index')->name('genderindex');

Route::get('/Gender/view/{id}', ['uses'=>'GenderController@view']);

Route::get('/Gender/edit/{id}', 'GenderController@edit');

Route::post('/Gender/update', ['uses'=>'GenderController@update']);

Route::get('/Gender/delete/{id}', ['uses'=>'GenderController@delete']);

Route::post('Gender/search_result', function(){

    $path = 'Gender/search/'. $_POST['keyword'];

    return redirect($path);

});

Route::get('/Gender/search/{keyword}', ['uses'=>'GenderController@search'])->name('gendersearchindex');


Route::get('/Gender/search/',function (){
    return redirect()->route('gendersearchindex');
});



Route::get('/Gender/',function (){
    return redirect()->route('genderindex');
});















Route::get('/hobbies', function () {
    return view('Hobbies/create');
});

Route::post('/Hobbies/store', ['uses'=>'HobbiesController@store']);

Route::get('/Hobbies/create',function (){

    return view('Hobbies/create');
} )->name('HobbiesCreate');

Route::get('/Hobbies/index', 'HobbiesController@index')->name('hobbiesindex');

Route::get('/Hobbies/view/{id}', ['uses'=>'HobbiesController@view']);

Route::get('/Hobbies/edit/{id}', 'HobbiesController@edit');

Route::post('/Hobbies/update', ['uses'=>'HobbiesController@update']);

Route::get('/Hobbies/delete/{id}', ['uses'=>'HobbiesController@delete']);

Route::post('Hobbies/search_result', function(){

    $path = 'Hobbies/search/'. $_POST['keyword'];

    return redirect($path);

});

Route::get('/Hobbies/search/{keyword}', ['uses'=>'HobbiesController@search'])->name('hobbiessearchindex');


Route::get('/Hobbies/search/',function (){
    return redirect()->route('hobbiessearchindex');
});



Route::get('/Hobbies/',function (){
    return redirect()->route('hobbiesindex');
});




















Route::get('/profile_picture', function () {
    return view('Profile_Picture/create');
});

Route::post('/Profile_Picture/store', ['uses'=>'ProfilePictureController@store']);


Route::get('/Profile_Picture/create',function (){

    return view('Profile_Picture/create');
} )->name('ProfilePictureCreate');

Route::get('/Profile_Picture/index', 'ProfilePictureController@index')->name('profilepictureindex');

Route::get('/Profile_Picture/view/{id}', ['uses'=>'ProfilePictureController@view']);

Route::get('/Profile_Picture/edit/{id}', 'ProfilePictureController@edit');

Route::post('/Profile_Picture/update', ['uses'=>'ProfilePictureController@update']);

Route::get('/Profile_Picture/delete/{id}', ['uses'=>'ProfilePictureController@delete']);

Route::post('Profile_Picture/search_result', function(){

    $path = 'Profile_Picture/search/'. $_POST['keyword'];

    return redirect($path);

});

Route::get('/Profile_Picture/search/{keyword}', ['uses'=>'ProfilePictureController@search'])->name('profilepicturesearchindex');


Route::get('/Profile_Picture/search/',function (){
    return redirect()->route('profilepicturesearchindex');
});



Route::get('/Profile_Picture/',function (){
    return redirect()->route('profilepictureindex');
});


















Route::get('/summary_of_organization', function () {
    return view('Summary_Of_Organization/create');
});

Route::post('/Summary_Of_Organization/store', ['uses'=>'SunmmaryOfOrganizationController@store']);

Route::get('/Summary_Of_Organization/create',function (){

    return view('Summary_Of_Organization/create');
} )->name('SummaryOfOrganizationCreate');

Route::get('/Summary_Of_Organization/index', 'SunmmaryOfOrganizationController@index')->name('summaryoforganizationindex');

Route::get('/Summary_Of_Organization/view/{id}', ['uses'=>'SunmmaryOfOrganizationController@view']);

Route::get('/Summary_Of_Organization/edit/{id}', 'SunmmaryOfOrganizationController@edit');

Route::post('/Summary_Of_Organization/update', ['uses'=>'SunmmaryOfOrganizationController@update']);

Route::get('/Summary_Of_Organization/delete/{id}', ['uses'=>'SunmmaryOfOrganizationController@delete']);

Route::post('Summary_Of_Organization/search_result', function(){

    $path = 'Summary_Of_Organization/search/'. $_POST['keyword'];

    return redirect($path);

});

Route::get('/Summary_Of_Organization/search/{keyword}', ['uses'=>'SunmmaryOfOrganizationController@search'])->name('summaryoforganizationsearchindex');


Route::get('/Summary_Of_Organization/search/',function (){
    return redirect()->route('summaryoforganizationsearchindex');
});



Route::get('/Summary_Of_Organization/',function (){
    return redirect()->route('summaryoforganizationindex');
});

